@extends('layouts.master')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice-title" style="display: flex; justify-content: space-between">
                        <h2 style="font-weight: bold;">Prochain lancement</h2>
                        <div class="mb-4">
                            <img src="image/logo.ico" alt="logo" height="60" />
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                                <h4 style="font-weight: 600">Nom du lancement :</h4><br>
                                <h5>{{$name}}</h5><br>

                        </div>
                        <div class="col-sm-6 text-sm-end">
                                <h4 style="font-weight: 600">Numéro de vol :</h4><br>
                                <h5>#{{$flightNumber}}</h5><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                                <h4 style="font-weight: 600">Nom de la rocket:</h4><br>
                                <h5>{{$rocketName}}</h5><br>

                        </div>
                        <div class="col-sm-6 text-sm-end">

                                <h4 style="font-weight: 600">Date prévue : </h4><br>
                                <h5>{{$date}}</h5><br>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                                <h4 style="font-weight: 600">Landpad :</h4><br>
                                @if($landpad != null)
                                    <h5>{{$landpad}}</h5><br>
                                @else
                                    <h5>Aucun</h5>
                                @endif

                        </div>
                        <div class="col-sm-6 text-sm-end">

                                <h4 style="font-weight: 600">Launchpad :</h4><br>

                                <h5>{{$launchpad}}</h5><br>

                        </div>
                    </div>
                    <div class="d-print-none">
                        <div class="float-end">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light me-1"><i
                                    class="fa fa-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
            integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
            crossorigin=""></script>

@endsection
