<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu" style="margin-top: 5%">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li style="margin-top: 10%;">
                    <a href="/" class="waves-effect">
                        <i class="bx bx-rocket"></i>
                        <span key="t-authentication">Liste des rockets par nombre de vol</span>
                    </a>
                </li>

                <li style="margin-top:5%;" class="menu-title" key="t-apps">------------------------</li>

                <li style="margin-top: 10%;">
                    <a href="/map" class="waves-effect">
                        <i class="bx bx-map-alt"></i>
                        <span key="t-dashboards">Map des land/launchpads</span>
                    </a>

                </li>

                <li style="margin-top:5%;" class="menu-title" key="t-apps">------------------------</li>

                <li style="margin-top: 10%;">
                    <a href="filterByLaunch" class="waves-effect">
                        <i class="bx bx-rocket"></i>
                        <span key="t-dashboards">Liste des rockets par launchpad</span>
                    </a>
                </li>
                <li style="margin-top: 10%;">
                    <a href="filterByLand" class="waves-effect">
                        <i class="bx bx-rocket"></i>
                        <span key="t-dashboards">Liste des rockets par landpad</span>
                    </a>
                </li>

                <li style="margin-top:5%;" class="menu-title" key="t-apps">------------------------</li>


                <li style="margin-top: 10%;">
                    <a href="nextLaunch" class="waves-effect">
                        <i class="bx bx-rocket"></i>
                        <span key="t-dashboards">Prochain lancement</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
