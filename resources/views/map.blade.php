@extends('layouts.master')

@section('content')
    <div style="display: flex">
        <h5 style="align-self: center;">Launchpad : </h5> <img  style="margin-left: 1%" src="image/fusee.png" width="30">
        <h5 style="align-self: center; margin-left: 2%">Landpad : </h5><img style="margin-left: 1%" src="image/fusee2.png" width="30">
    </div>
    <div id="map"></div>

@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
            integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
            crossorigin=""></script>
    <script>

        let map = L.map('map').setView([31.505, -90.09], 13);
        let landpad = L.icon({
            iconUrl: 'image/fusee2.png',
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [0, -30],
        });

        let launchpad = L.icon({
            iconUrl: 'image/fusee.png',
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [0, -30],
        });
        let url = 'https://api.spacexdata.com/v4/launchpads';

        fetch(url).then(function (res) {
            if (res.ok) {
                return res.json();
            }
        }).then(function (value) {
            value.forEach(element => {

                L.marker([element.latitude, element.longitude], {icon: launchpad}).addTo(map).bindPopup("Plateforme de lancement : " + element.name).openPopup();
            });
            console.log(value);

        }).catch(function (err) {
            console.error(err);
        });

        let url2 = 'https://api.spacexdata.com/v4/landpads';

        fetch(url2).then(function (res) {
            if (res.ok) {
                return res.json();
            }
        }).then(function (value) {
            value.forEach(element => {

                L.marker([element.latitude, element.longitude], {icon: landpad}).addTo(map).bindPopup("Plateforme d'atterrissage : " + element.name)
            });
            console.log(value);

        }).catch(function (err) {
            console.error(err);
        });


        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

    </script>
@endsection
