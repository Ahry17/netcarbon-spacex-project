@extends('layouts.master')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Liste des rockets par nombre de vol</h4>
                <table id="table_id" class="table table-bordered nowrap w-100">
                    <thead>
                    <tr>
                        <th>Plate-forme de lancement</th>
                        <th>Fusée</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($launchpads as $launchpad)
                        <tr>
                            <td>{{$launchpad->name}}</td>
                            <td>
                                <table>
                                    @foreach($launchpad->rockets as $rocket)
                                        <tr>
                                            <td>{{$rocketListName[$rocket]}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>

    <script>

        let table = $('#table_id');
        $(document).ready(function () {
            table.DataTable({

                language: {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                responsive: true,
            });

            $('#search').keyup(function () {
                console.log("test")
                table.DataTable().search($(this).val()).draw();
            })
        });

    </script>
@endsection
