
# **SpaceX Search**
 ![](https://gitlab.com/Ahry17/netcarbon-spacex-project/-/raw/main/public/image/logo.ico)


### Liste des rockets par nombre de vol

Cette première page contient les rockets de spaceX ainsi que leur nombre de vol par année. De plus des informations sur les rockets, comme le coût de lancement ou le pays. Une barre de recherche permet la recherche instantanée parmi tous les critères présents dans la table.
Voici un exemple : 

 ![](https://gitlab.com/Ahry17/netcarbon-spacex-project/-/raw/main/public/image/cap.png)


### Map des land/launchpads 

Menu simple avec une map sur laquelle on peut zoomer, et cliquer sur les différents landpads et launchpads, permettant l'affichage de son nom.

### Liste des rockets par launchpads et par landpads

Deux pages différentes, mais avec le même but, celui de référencer les rockets suivant leurs différents landpads et launchpads. La barre de recherche est encore présente et permet le filtre instantané.

### Prochain lancement 

Cette dernière page répertorie les informations pertinentes du prochain lancement de la fusée SpaceX.
