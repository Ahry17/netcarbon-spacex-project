<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $rockets = Http::get('https://api.spacexdata.com/v4/rockets')->object();
    $rocketArrayWithDate = array(array());
//    init rocketArrayWithDate

    foreach ($rockets as $rocket) {
        $rocketArrayWithDate[$rocket->name] = [];
    }
    //dd($rocketArrayWithDate);
    foreach ($rockets as $rocket) {
        $response = Http::withBody('{"query":{"rocket":"' . $rocket->id . '"},"options":{"limit":100000,"select":"date_utc"}}', 'application/json')->post('https://api.spacexdata.com/v4/launches/query');
        $flights = $response->json()['docs'];
        foreach ($flights as $flight) {
            $year = substr($flight['date_utc'], 0, 4);
            if (!isset($rocketArrayWithDate[$rocket->name][$year])) {
                $rocketArrayWithDate[$rocket->name][$year] = 1;
            } else {
                $rocketArrayWithDate[$rocket->name][$year]++;
            }
        }
    }
//    dd($rocketArrayWithDate);
    return view('home', compact('rockets', 'rocketArrayWithDate'));
});

Route::get('/map', function () {
    return view('map');
});

Route::get('/filterByLaunch', function () {

    $launchpads = Http::get('https://api.spacexdata.com/v4/launchpads')->object();
    $rockets = Http::get('https://api.spacexdata.com/v4/rockets')->object();

    $rocketListName = array();
    //to translate id to name of rockets
    foreach ($rockets as $rocket) {
        $rocketListName[$rocket->id] = $rocket->name;
    }
    return view('filterByLaunch', compact('launchpads', 'rocketListName'));
});

Route::get('/filterByLand', function () {

    $landpads = Http::get('https://api.spacexdata.com/v4/landpads')->object();

    $launches = Http::get('https://api.spacexdata.com/v4/launches')->object();
    $listRocketPerLandpad = array();

    $rockets = Http::get('https://api.spacexdata.com/v4/rockets')->object();
    $rocketListName = array();
    // init array with key as landapd and as value of array of possible rocket
    foreach ($landpads as $landpad)
    {
        $listRocketPerLandpad[$landpad->id] = [];
    }
    //to translate id to name of rockets
    foreach ($rockets as $rocket) {
        $rocketListName[$rocket->id] = $rocket->name;
    }
    foreach ($launches as $launch) {

        foreach ($launch->cores as $core)
        {
            if($core->landpad != null) // if landpad exist
            {
                $listRocketPerLandpad[$core->landpad][] = $rocketListName[$launch->rocket];
                $listRocketPerLandpad[$core->landpad] = array_unique($listRocketPerLandpad[$core->landpad]);
            }
        }
    }
    //dd($listRocketPerLandpad);


    return view('filterByLand', ['landpads' => $landpads], ['listRocketPerLandpad' => $listRocketPerLandpad]);
});

Route::get('/nextLaunch', function () {

    $nextLaunch = Http::get('https://api.spacexdata.com/v4/launches/next')->object();

    //to translate id to name of rockets
    $rockets = Http::get('https://api.spacexdata.com/v4/rockets')->object();
    $rocketListName = array();
    foreach ($rockets as $rocket) {
        $rocketListName[$rocket->id] = $rocket->name;
    }

    //to translate id to name of landpad
    $launchpads = Http::get('https://api.spacexdata.com/v4/launchpads')->object();
    $launchpadListName = array();
    foreach ($launchpads as $launchpad) {
        $launchpadListName[$launchpad->id] = $launchpad->name;
    }

    $rocketName = $rocketListName[$nextLaunch->rocket];
    $launchpad = $launchpadListName[$nextLaunch->launchpad];
    $details = $nextLaunch->details;
    $date = Carbon::parse($nextLaunch->date_utc)->format('Y-d-m');
    $name = $nextLaunch->name;
    $flightNumber = $nextLaunch->flight_number;
    $landpad = $nextLaunch->cores[0]->landpad;
    return view('nextLaunch', compact('rocketName','launchpad', 'date', 'name', 'flightNumber', 'landpad'));
});
